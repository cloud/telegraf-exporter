# Cassandra 
This project is using a cassandra base image in order to query a CassandraDB and collect metrics about the Tungsten metrics.
The query (JSON) is parsed and pushed to InfluxDB using the built in JSON tool of telegraf. 

The cassandra image is pushed to `registry.cern.ch/cloud/telegraf-exporter/cassandra`. 
